/**
 * genie - filesystem tagger
 *
 * Copyright 2017 zachwick <zach@zachwick.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **/

extern crate clap;
extern crate rusqlite;
extern crate time;

use clap::{Arg, App, SubCommand};
use rusqlite::{Connection, Statement, Row, Rows};
use time::Timespec;
use std::fs;
use std::path::PathBuf;
use std::path::Path;

#[derive(Debug)]
struct TagPair {
    id: i32,
    path: String,
    tag: String,
    time_created: Timespec
}

fn main() {
    let matches = App::new("genie")
        .version("1.0.0")
        .about("filesystem tagger")
        .author("zach wick")
        .subcommand(SubCommand::with_name("init")
                    .about("initialize genie. This is only required once"))
        .subcommand(SubCommand::with_name("tag")
                    .about("tag the given PATH with the given TAG")
                    .arg(Arg::with_name("path")
                         .required(true)
                         .index(1))
                    .arg(Arg::with_name("tag")
                         .required(true)
                         .index(2)))
        .subcommand(SubCommand::with_name("rm")
                    .about("remove from the given PATH the given TAG")
                    .arg(Arg::with_name("path")
                         .required(true)
                         .index(1))
                    .arg(Arg::with_name("tag")
                         .required(true)
                         .index(2)))
        .subcommand(SubCommand::with_name("search")
                    .about("search for and return all PATHS that have TAG")
                    .arg(Arg::with_name("tag")
                         .required(true)
                         .index(1)))
        .subcommand(SubCommand::with_name("show")
                    .about("show all tags applied to the given PATH")
                    .arg(Arg::with_name("path")
                         .required(true)
                         .index(1)))
        .get_matches();

    // This should be modified to wherever you would like your genie db to live
    let dbpath = Path::new("/Users/zwick/.geniedb");

    if let Some(matches) = matches.subcommand_matches("init") {
        //let dbpath = PathBuf::from("~/.geniedb");
        let conn = Connection::open(dbpath).unwrap();
        conn.execute("CREATE TABLE IF NOT EXISTS genie (
                      id INTEGER PRIMARY KEY,
                      path TEXT NOT NULL,
                      tag TEXT NOT NULL,
                      time_created TEXT NOT NULL
                     )", &[]).unwrap();
        println!("genie initialized");
    } else if let Some(matches) = matches.subcommand_matches("tag") {
        let conn = Connection::open(dbpath).unwrap();
        let relpath = PathBuf::from(matches.value_of("path").unwrap().to_string());
        let newtag = TagPair {
            id: 0,
            path: fs::canonicalize(&relpath).unwrap().into_os_string().into_string().unwrap(),
            tag: matches.value_of("tag").unwrap().to_string(),
            time_created: time::get_time()
        };
        conn.execute("INSERT INTO genie (path, tag, time_created)
                  VALUES (?1, ?2, ?3)",
                 &[&newtag.path, &newtag.tag, &newtag.time_created]).unwrap();
    } else if let Some(matches) = matches.subcommand_matches("rm") {
        let conn = Connection::open(dbpath).unwrap();
        let relpath = PathBuf::from(matches.value_of("path").unwrap().to_string());
        let fullpath = fs::canonicalize(&relpath).unwrap().into_os_string().into_string().unwrap();
        let rmtag = matches.value_of("tag").unwrap().to_string();

        conn.execute("DELETE FROM genie WHERE path=?1 and tag=?2",
                     &[&fullpath, &rmtag]).unwrap();
    } else if let Some(matches) = matches.subcommand_matches("search") {
        let conn = Connection::open(dbpath).unwrap();
        let tag = matches.value_of("tag").unwrap().to_string();
        let mut stmt = conn.prepare("SELECT DISTINCT path FROM genie WHERE tag=:tag").unwrap();
        let mut rows = stmt.query_named(&[(":tag", &tag)]).unwrap();
        while let Some(row) = rows.next() {
            let path:String = row.unwrap().get(0);
            println!("{}", path);
        }
    } else if let Some(matches) = matches.subcommand_matches("show") {
        let conn = Connection::open(dbpath).unwrap();
        let relpath = PathBuf::from(matches.value_of("path").unwrap().to_string());
        let fullpath = fs::canonicalize(&relpath).unwrap().into_os_string().into_string().unwrap();
        let mut stmt = conn.prepare("SELECT DISTINCT tag FROM genie WHERE path=:path").unwrap();
        let mut rows = stmt.query_named(&[(":path", &fullpath)]).unwrap();
        while let Some(row) = rows.next() {
            let tag:String = row.unwrap().get(0);
            println!("{}", tag);
        }
    }
}
