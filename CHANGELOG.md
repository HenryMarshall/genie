### 1.0.0 2017-04-20

* Initial launch of genie
* Implements basic 'tag', 'rm', 'search', and 'show' commands
